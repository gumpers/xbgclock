X11 digital clock (with bonus system monitor)
Based on https://github.com/LizzyFleckenstein03/clockoverlay
_______
Libraries required to build:
```
libx11-dev
libxext-dev
libxft-dev
x11proto-dev
```
Displays date and time in the top left corner of the screen and temperature/cpu usage/ram usage in the top right. 
```make clock``` builds just the clock part and ```make bar``` builds the whole thing.
Refreshes roughly every 20 seconds. Send the process a SIGUSR1 to refresh immediately.
